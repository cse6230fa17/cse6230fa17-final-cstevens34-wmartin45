
RM=rm -f

all: syllabus.pdf

syllabus.pdf: README.md Makefile
	pandoc -o $@ -f markdown+pipe_tables $< -V geometry:margin=1in --wrap=none

clean:
	$(RM) syllabus.pdf

.PHONY: clean
