# git & Bitbucket

Students who are new to [git][gitscm] should read up with this [tutorial][tutorial].

## Configuring git

Some things you probably want in your git configuration for all of your projects:

* Set your name: `git config --global user.name "George P. Burdell"`
* Set your email: `git config --global user.email "me@example.com"`
* Do not push local branches nonexistent on upstream by default: `git config --global push.default simple` (older versions of git require `git config --global push.default tracking`)

## Workflow for this class

When there is an assignment (exercise or project) that you must use bitbucket to complete:

1. Go to the repo for the assignment (e.g., [exercise 04](https://bitbucket.org/cse6230fa17/ex04/)
2. Fork it, and give it a name of the form `cse6230fa17-ex04-gtusername`
3. Make your fork private
4. Get a local working copy of your bitbucket repo: `git clone ssh://git@bitbucket.org/youraccount/cse6230fa17-ex04-gtusername.git cse6230fa17-ex04`
5. Get a working copy on any computing platform where you need to run your code, like [Deepthought](https://support.cc.gatech.edu/facilities/instructional-labs/deepthought-cluster)
6. Commit your changes.
7. When you're reading to submit, push your latest changes to your bitbucket repo
8. Transfer ownership of the repository to the `cse6230fa17` team

## Additional Reading

* [Git tutorial from Atlassian][tutorial]

[gitscm]: https://git-scm.com/ "Git"
[tutorial]: https://www.atlassian.com/git/tutorials "Git Tutorials and Training (Atlassian)"
