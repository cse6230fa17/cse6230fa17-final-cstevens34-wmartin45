# TAU on stampede2

- `module load tau`

- ~~Compile code with `MPICC=taucc`~~ `taucc` broken on `stampede2`

- Run with environment variable `TAU_TRACE=1` and replace `./my_prog` with `tau_exec ./my_prog` (after `mpirun` type commands)

- Many trace files will be created in the directory where it is run

- `tau_treemerge.pl` will create `tau.trc` and `tau.edu`

- `tau2slog2 tau.trc tau.edf -o tau.slog2`

- Copy `tau.slog2` to your local routine

    - `man scp` to figure out how to transfer files
    - Either have your password for `stampede2` or set up an authorized `ssh` key

- Install [tau](https://www.cs.uoregon.edu/research/tau/downloads.php) on your machine
    - `configure`, `make`, `make install`
    - Sometimes `tau` is available as a package, but doesn't usually work with MPI out of the box
    - Should include the `jumpshot` program
    - Unfortunately, may need to have an up-to-date version of Java (e.g., `sudo apt-get install openjdk-8-jre`)

- `jumpshot tau2slog2`
