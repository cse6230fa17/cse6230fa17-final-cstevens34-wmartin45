
#include <stdio.h>
#include <unistd.h>

int programdata[500000000] = {0};

int main (int argc, char **argv)
{
  pid_t fork_id = 0, my_id;

  /* comment this out and see how the memory usage changes */
  fork_id = fork();

  my_id = getpid();

  if (!fork_id) {
    sleep(1);
    printf("I am the child, my PID is %d and my PPID is %d\n", my_id, getppid());
  } else {
    printf("I am the parent, my PID is %d and my child's pid is %d\n", my_id, fork_id);
  }

  if (argc > 1) {
    for (size_t i = 0; i < 500000000; i++) {
      programdata[i] = my_id;
    }
  }

  sleep(10);

  if (!fork_id) {
    printf("PID read from data %d\n", programdata[0]);
  } else {
    sleep(1);
    printf("PID read from data %d\n", programdata[0]);
  }

  return 0;
}
