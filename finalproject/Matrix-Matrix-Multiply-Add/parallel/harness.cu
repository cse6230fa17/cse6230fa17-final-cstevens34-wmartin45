#include <stdio.h>
#include <curand.h>
#include "mmma.h"
#include <petscsys.h>
#include <cublas_v2.h>


void __global__ scaleRandom(int count, double *array)
{
  int tid      = blockDim.x * blockIdx.x + threadIdx.x;
  int gridSize = gridDim.x * blockDim.x;

  for (int i = tid; i < count; i += gridSize) {
    array[i] = 2. * array[i] - 1.;
  }
}

void __global__ arrayAbs(int count, double *array)
{
  int tid      = blockDim.x * blockIdx.x + threadIdx.x;
  int gridSize = gridDim.x * blockDim.x;

  for (int i = tid; i < count; i += gridSize) {
    array[i] = fabs(array[i]);
  }
}

void __global__ arrayScale(int count, double scale, double *array)
{
  int tid      = blockDim.x * blockIdx.x + threadIdx.x;
  int gridSize = gridDim.x * blockDim.x;

  for (int i = tid; i < count; i += gridSize) {
    array[i] *= scale;
  }
}

static void print_matrix(double *matrix, int rows, int cols, int start_row, int end_row, int start_col, int end_col)
{
  int row_size = (end_col - start_col);
  for (int r = 0; r < rows; r++)
  {
    printf(" %3d: ", r);
    for (int c = 0; c < cols; c++)
    {
      if ((r >= start_row && r < end_row) && (c >= start_col && c < end_col))
      {
        printf(" %4.1f", matrix[(r-start_row)*row_size + c-start_col]);
      }
      else
        printf(" ----");
    }
    printf("\n");
  }
  printf(">>>>> end matrix <<<<<\n");
  fflush(stdout);
}

static double* getMatrixBlock(int size, double* d_array)
{
  double* array = (double*)malloc(size * sizeof(double));
  cudaMemcpy(array, d_array, size * sizeof(double), cudaMemcpyDeviceToHost);
  return array;
}

static void setMatrixBlockInc(int localSize, double val, double *d_array)
{
  int v = 0;
  double* array = (double*)malloc(localSize * sizeof(double));
  for (int i = 0; i < localSize; i++) { array[i] = ++v; }

  cudaMemcpy(d_array, array, localSize * sizeof(double), cudaMemcpyHostToDevice);
  free(array);
}

static void setMatrixBlock(int localSize, double val, double *d_array)
{
  double* array = (double*)malloc(localSize * sizeof(double));
  for (int i = 0; i < localSize; i++) { array[i] = val; }

  cudaMemcpy(d_array, array, localSize * sizeof(double), cudaMemcpyHostToDevice);
  free(array);
}

static void setRandomMatrixBlock(int localSize, curandGenerator_t gen, dim3 block, double *d_array)
{
  dim3 grid;

  curandGenerateUniformDouble(gen, d_array, localSize);
  grid = dim3((localSize + block.x - 1) / block.x);
  scaleRandom <<<grid, block>>>(localSize, d_array);
}


static void setup_curand(curandGenerator_t** gen, int numDevices) {
  *gen = (curandGenerator_t*) malloc(numDevices * sizeof(curandGenerator_t));
  for (int d = 0; d < numDevices; d++) {
    cudaSetDevice(d);
    curandCreateGenerator(&(*gen)[d], CURAND_RNG_PSEUDO_DEFAULT);
    curandSetPseudoRandomGeneratorSeed((*gen)[d],d);
  }
}

static void teardown_curand(curandGenerator_t** gen, int numDevices) {
  for (int d = 0; d < numDevices; d++) {
    cudaSetDevice(d);
    curandDestroyGenerator((*gen)[d]);
  }
  free(*gen);
}

static void setup_blocks(int numDevices,
    struct MatrixBlock** cBlock, struct MatrixBlock** aBlock, struct MatrixBlock** bBlock) {
  *cBlock = (struct MatrixBlock*) malloc(numDevices * sizeof(struct MatrixBlock));
  *aBlock = (struct MatrixBlock*) malloc(numDevices * sizeof(struct MatrixBlock));
  *bBlock = (struct MatrixBlock*) malloc(numDevices * sizeof(struct MatrixBlock));
}

static void teardown_blocks(struct MatrixBlock** cBlock, struct MatrixBlock** aBlock, struct MatrixBlock** bBlock) {
  free(*cBlock);
  free(*aBlock);
  free(*bBlock);
}

static void setup_matrices(int numDevices,
    double **C[], double **A[], double **B[],
    double **D[], double **E[]) {
  *C = (double**) malloc(numDevices * sizeof(double*));
  *A = (double**) malloc(numDevices * sizeof(double*));
  *B = (double**) malloc(numDevices * sizeof(double*));
  *D = (double**) malloc(numDevices * sizeof(double*));
  *E = (double**) malloc(numDevices * sizeof(double*));
}

static void teardown_matrices(double **C[], double **A[], double **B[],
    double **D[], double **E[]) {
  free(*C);
  free(*A);
  free(*B);
  free(*D);
  free(*E);
}

static PetscErrorCode MMMAApply(PetscInt M, PetscInt N, PetscInt R, PetscInt numDevices,
                                PetscScalar alpha,
                                const struct MatrixBlock *cBlock,
                                const struct MatrixBlock *aBlock,
                                const struct MatrixBlock *bBlock,
                                PetscScalar *C[], PetscScalar *A[], PetscScalar *B[])
{
  int                   block, grid;
  cudaError_t           cerr;
  PetscErrorCode        ierr;
  struct cudaDeviceProp prop;

  PetscFunctionBegin;
  cerr = cudaGetDeviceProperties(&prop, 0); 
  block = prop.maxThreadsPerBlock;
  for (int i = 0; i < numDevices; i++) {
    PetscScalar    *c = C[i];
    PetscInt       rowC = (cBlock[i].rowEnd - cBlock[i].rowStart);
    PetscInt       colC = (cBlock[i].colEnd - cBlock[i].colStart);
    PetscInt       nC = rowC * colC;
    cublasHandle_t handle;
    cublasStatus_t cbs;

    cerr = cudaSetDevice(i); 
    grid = (nC + block - 1) / block;
    /* scale C separately, because we may have to add multiple gemm contributions together */
    arrayScale<<<grid, block>>>(nC, alpha, C[i]);
    cbs = cublasCreate(&handle); 
    for (int j = 0; j < numDevices; j++) {
      PetscInt mStart, mEnd;
      PetscInt rowA = (aBlock[j].rowEnd - aBlock[j].rowStart);
      PetscInt colA = (aBlock[j].colEnd - aBlock[j].colStart);
      PetscInt nA = rowA * colA;

      mStart = PetscMax(cBlock[i].rowStart, aBlock[j].rowStart);
      mEnd   = PetscMin(cBlock[i].rowEnd,   aBlock[j].rowEnd);
      if (mStart >= mEnd) continue;
      for (int k = 0; k < numDevices; k++) {
        PetscInt nStart, nEnd, rStart, rEnd;
        PetscInt rowB = (bBlock[k].rowEnd - bBlock[k].rowStart);
        PetscInt colB = (bBlock[k].colEnd - bBlock[k].colStart);
        PetscInt nB = rowB * colB;
        PetscScalar *a, *b, one = 1., one2 = 1.;

        nStart = PetscMax(cBlock[i].colStart, bBlock[k].colStart);
        nEnd   = PetscMin(cBlock[i].colEnd,   bBlock[k].colEnd);
        if (nStart >= nEnd) continue;
        rStart = PetscMax(aBlock[j].colStart, bBlock[k].rowStart);
        rEnd   = PetscMin(aBlock[j].colEnd,   bBlock[k].rowEnd);
        if (rStart >= rEnd) continue;
        printf("qGEMM block: [%i:%i,%i:%i] (%i) += [%i:%i,%i:%i] (%i) * [%i:%i,%i:%i] (%i)\n",
                           mStart, mEnd, nStart, nEnd, i, mStart, mEnd, rStart, rEnd, j, rStart, rEnd, nStart, nEnd, k); 
        if (i == j) {
          a = A[j];
        } else {
          cerr = cudaMalloc(&a,nA * sizeof(PetscScalar)); 
          cerr = cudaMemcpyPeer(a, i, A[j], j, nA * sizeof(PetscScalar)); 
        }
        if (i == k) {
          b = B[k];
        } else {
          cerr = cudaMalloc(&b,nB * sizeof(PetscScalar)); 
          cerr = cudaMemcpyPeer(b, i, B[k], k, nB * sizeof(PetscScalar)); 
        }

        /* now we have the necessary blocks on device i: call cuBLAS */
        cbs = cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N,
                          (nEnd - nStart), (mEnd - mStart), (rEnd - rStart),
                          &one,
                          &b[colB * (rStart - bBlock[k].rowStart) + nStart - bBlock[k].colStart],
                          colB,
                          &a[colA * (mStart - aBlock[j].rowStart) + rStart - aBlock[j].colStart],
                          colA,
                          &one2,
                          &c[colC * (mStart - cBlock[i].rowStart) + nStart - cBlock[i].colStart],
                          colC); 
        if (i != k) {
          cerr = cudaFree(b); 
        }
        if (i != j) {
          cerr = cudaFree(a); 
        }
      }
    }
    cbs = cublasDestroy(handle); 
  }
  cerr = cudaSetDevice(0); 
  PetscFunctionReturn(0);
}

static PetscErrorCode TestMMMACheck(PetscInt M, PetscInt N, PetscInt R, PetscInt numDevices,
                                    PetscScalar alpha,
                                    const struct MatrixBlock *cBlock,
                                    const struct MatrixBlock *aBlock,
                                    const struct MatrixBlock *bBlock,
                                    PetscScalar *C[], PetscScalar *A[], PetscScalar *B[],
                                    PetscScalar *D[], PetscScalar *E[])
{
  struct cudaDeviceProp prop;
  cudaError_t    cerr;
  int            block, grid;
  PetscReal      margin = PetscPowReal(((PetscReal) 1. + (PetscReal) PETSC_MACHINE_EPSILON),2. * (2. * R + 1)) - 1.;
  PetscReal      maxThresh = 0.;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = MMMAApply(M, N, R, numDevices, alpha, cBlock, aBlock, bBlock, D, A, B); CHKERRQ(ierr);
  cerr = cudaGetDeviceProperties(&prop, 0); 
  block = prop.maxThreadsPerBlock;
  for (int i = 0; i < numDevices; i++) {
    PetscInt arraySize;

    cerr = cudaSetDevice(i); 

    arraySize = (aBlock[i].rowEnd - aBlock[i].rowStart) * (aBlock[i].colEnd - aBlock[i].colStart);
    grid = (arraySize + block - 1) / block;
    arrayAbs<<<grid, block>>>(arraySize, A[i]);

    arraySize = (bBlock[i].rowEnd - bBlock[i].rowStart) * (bBlock[i].colEnd - bBlock[i].colStart);
    grid = (arraySize + block - 1) / block;
    arrayAbs<<<grid, block>>>(arraySize, B[i]);
  }
  cerr = cudaSetDevice(0); 

  ierr = MMMAApply(M, N, R, numDevices, PetscAbsReal(alpha), cBlock, aBlock, bBlock, E, A, B); CHKERRQ(ierr);

  for (int i = 0; i < numDevices; i++) {
    PetscInt rowC, colC, arraySize;
    PetscScalar *h_C, *h_D, *h_E;

    cerr = cudaSetDevice(i); 

    rowC = (cBlock[i].rowEnd - cBlock[i].rowStart);
    colC = (cBlock[i].colEnd - cBlock[i].colStart);
    arraySize = rowC * colC;

    ierr = PetscMalloc3(arraySize, &h_C, arraySize, &h_D, arraySize, &h_E); CHKERRQ(ierr);

    cerr = cudaMemcpy(h_C,C[i],arraySize*sizeof(PetscScalar),cudaMemcpyDeviceToHost); CHKERRQ(ierr);
    cerr = cudaMemcpy(h_D,D[i],arraySize*sizeof(PetscScalar),cudaMemcpyDeviceToHost); CHKERRQ(ierr);
    cerr = cudaMemcpy(h_E,E[i],arraySize*sizeof(PetscScalar),cudaMemcpyDeviceToHost); CHKERRQ(ierr);

    for (int j = 0; j < arraySize; j++) {
      PetscReal res = h_D[j] - h_C[j];
      PetscReal thresh = margin * h_E[j];

      maxThresh = PetscMax(thresh,maxThresh);
      if (PetscAbsReal(res) > thresh) {
        SETERRQ7(PETSC_COMM_SELF, PETSC_ERR_LIB, "Failed error test at (%D,%D) threshold %g with value %g, (C %g, D %g, E %g)\n", (j / colC) + cBlock[i].rowStart, (j % colC) + cBlock[i].colStart, (double) thresh, (double) res, (double) h_C[j], (double) h_D[j], (double) h_E[j]);
      }
    }
    ierr = PetscFree3(h_C, h_D, h_E); CHKERRQ(ierr);
  }
  cerr = cudaSetDevice(0); 
  ierr = PetscPrintf(PETSC_COMM_SELF, "Maximum threshold: %g\n", (double) maxThresh);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "Passed.\n");CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int main(int argc, char **argv) {
  int numDevices = 0;
  struct MatrixBlock *cBlock, *aBlock, *bBlock;
  double **A, **B, **C, **D, **E;
  curandGenerator_t *gen;

  printf("start\n");
  cudaGetDeviceCount(&numDevices);
  // if (numDevices) numDevices = 1;
  printf("numDevices=%i\n", numDevices);
  printf("doing setups\n");
  setup_curand(&gen, numDevices);
  setup_blocks(numDevices, &cBlock, &aBlock, &bBlock);
  setup_matrices(numDevices, &C, &A, &B, &D, &E);
  printf("done setups\n");

  size_t m = 100;
  size_t n = 10;
  size_t r = 5;
  double alpha = 3.5;
  printf("m=%zu, n=%zu, r=%zu, alpha=%f\n", m, n, r, alpha);

  printf("GetMatrix\n");
  MMMAGetMatrixArraysDoubleDevice(NULL, m, n, r, numDevices, cBlock, aBlock, bBlock, C, A, B);

  for (int d = 0; d < numDevices; d++) {
    int nClocal, nAlocal, nBlocal;
    dim3     block, grid;
    struct cudaDeviceProp prop;

    printf("setRandomMatrixBlock device=%i\n", d);
    cudaSetDevice(d);
    cudaGetDeviceProperties(&prop, d);
    block = dim3(prop.maxThreadsPerBlock);
    nClocal = (cBlock[d].rowEnd - cBlock[d].rowStart) * (cBlock[d].colEnd - cBlock[d].colStart);
    nAlocal = (aBlock[d].rowEnd - aBlock[d].rowStart) * (aBlock[d].colEnd - aBlock[d].colStart);
    nBlocal = (bBlock[d].rowEnd - bBlock[d].rowStart) * (bBlock[d].colEnd - bBlock[d].colStart);
    printf("nClocal=%i, nAlocal=%i, nBlocal=%i\n", nClocal, nAlocal, nBlocal);
    if (nClocal){
      // setRandomMatrixBlock(nClocal, gen[d], block, C[d]);
      setMatrixBlock(nClocal, d+1, C[d]);
    }
    if (nAlocal){
      // setRandomMatrixBlock(nAlocal, gen[d], block, A[d]);
      setMatrixBlock(nAlocal, 1, A[d]);
    }
    if (nBlocal) {
      // setRandomMatrixBlock(nBlocal, gen[d], block, B[d]);
      setMatrixBlock(nBlocal, 2, B[d]);
    }
    printf("C=%p\n", (void*)C[d]);
    printf("malloc and memcpy for D and E\n");
    cudaMalloc(&D[d], nClocal * sizeof(double));
    cudaMalloc(&E[d], nClocal * sizeof(double));
    cudaMemcpy(D[d], C[d], nClocal * sizeof(double), cudaMemcpyDeviceToDevice);
    cudaMemcpy(E[d], C[d], nClocal * sizeof(double), cudaMemcpyDeviceToDevice);
    grid = dim3((nClocal + block.x - 1)/block.x);
    printf("arrayAbs(E)\n");
    arrayAbs<<<grid, block>>>(nClocal, E[d]);
  }
  printf("MMMAApply start\n");
    fflush(stdout);
  MMMAApplyDoubleDevice(NULL, m, n, r, alpha, numDevices, cBlock, aBlock, bBlock, C, (const double**)A, (const double**)B);
  printf("MMMAApply finish\n");
    fflush(stdout);

  for (int d = 0; d < numDevices; d++)
  {
    cudaSetDevice(d);
    int nClocal = (cBlock[d].rowEnd - cBlock[d].rowStart) * (cBlock[d].colEnd - cBlock[d].colStart);
    double *mC = getMatrixBlock(nClocal, C[d]);

    print_matrix(mC, m, n, cBlock[d].rowStart, cBlock[d].rowEnd, cBlock[d].colStart, cBlock[d].colEnd);
    free(mC);
  }


  TestMMMACheck(m, n, r, numDevices, alpha, cBlock, aBlock, bBlock, C, A, B, D, E);

  printf("RestoreMatrix\n");
  MMMARestoreMatrixArraysDoubleDevice(NULL, m, n, r, numDevices, cBlock, aBlock, bBlock, C, A, B);

  printf("doing teardowns\n");
  teardown_matrices(&C, &A, &B, &D, &E);
  teardown_blocks(&cBlock, &aBlock, &bBlock);
  teardown_curand(&gen, numDevices);
  printf("done teardowns\n");
  return 0;
}
/* vi: set expandtab sw=2 ts=2 cindent: */
