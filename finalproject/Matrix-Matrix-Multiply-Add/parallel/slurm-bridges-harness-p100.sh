#!/bin/sh
#SBATCH  -J harness-p100             # Job name
#SBATCH  -p GPU                   # Queue (RM, RM-shared, GPU, GPU-shared)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --gres=gpu:p100:2                # GPU type and amount
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -o harness-p100-%j.out      # Standard output and error log
ulimit -c unlimited
pwd; hostname; date
module load pgi
module load cuda
module load petsc
#pgaccelinfo
#export PGI_ACC_TIME=1
#export PGI_ACC_NOTIFY=1
#export PGI_ACC_DEBUG=1
./harness
date
