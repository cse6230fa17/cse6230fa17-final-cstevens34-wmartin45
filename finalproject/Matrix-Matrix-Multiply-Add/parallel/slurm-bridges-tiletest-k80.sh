#!/bin/sh
#SBATCH  -J tiletest-p100             # Job name
#SBATCH  -p GPU-shared                   # Queue (RM, RM-shared, GPU, GPU-shared)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --gres=gpu:k80:4                # GPU type and amount
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -o tiletest-k80-%j.out      # Standard output and error log
pwd; hostname; date
module load pgi
#pgaccelinfo
pgcc -mp -acc -fast ta=tesla:cc35,cc60 -Minfo=accel -DCHECK  matrix-acc-tile.c -o matrix-acc-tile-ompcheck
export PGI_ACC_TIME=1
./matrix-acc-tile-ompcheck
date
