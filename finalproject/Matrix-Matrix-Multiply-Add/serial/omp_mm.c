#include <cse6230rand.h>
#include <stdio.h>
#include <omp.h>

cse6230nrand_t _nrand;
cse6230nrand_t *nrand = &_nrand;
unsigned int _seed = 1889;

void matmul_depend(int N, int BS, float A[N][N], float B[N][N],
    float C[N][N])
{
  int i, j, k, ii, jj, kk;
  int ctr = 0;
  for (i = 0; i < N; i+=BS) {
    for (j = 0; j < N; j+=BS) {
      for (k = 0; k < N; k+=BS) {
        ctr++;
        // Note 1: i, j, k, A, B, C are firstprivate by default
        // // Note 2: A, B and C are just pointers
#pragma omp task private(ii, jj, kk) \
        depend ( in: A[i:BS][k:BS], B[k:BS][j:BS] ) \
        depend ( inout: C[i:BS][j:BS] )
        {
          //int t_num = omp_get_thread_num();
          //printf("starting thread %i, ctr=%i\n", t_num, ctr);
          for (ii = i; ii < i+BS; ii++ )
            for (jj = j; jj < j+BS; jj++ )
              for (kk = k; kk < k+BS; kk++ )
                C[ii][jj] = C[ii][jj] + A[ii][kk] * B[kk][jj];
          //printf("  ending thread %i, ctr=%i\n", t_num, ctr);
        }
      }
    }
  }
}

void matmul_linear(int N, float A[N][N], float B[N][N],
    float C[N][N])
{
  for (int row = 0; row < N; row++)
  {
    for (int col = 0; col < N; col++)
    {
      for (int c = 0; c < N; c++)
      {
        C[row][col] += A[row][c] * B[c][col];
      }
    }
  }
}

void main() {
  const int N = 1000;
  float a[N][N];
  float b[N][N];
  float c[N][N];
  double start, end;

  cse6230nrand_seed(_seed,nrand);
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      a[i][j] = cse6230nrand(nrand);
      b[i][j] = cse6230nrand(nrand);
      c[i][j] = 0;
    }
  }
  printf("calling\n");
  start = omp_get_wtime();
  matmul_linear(N, a, b, c);
  end = omp_get_wtime();
  printf("Work took %f seconds\n", end - start);

  int bs = 50;
  printf("calling\n");
  start = omp_get_wtime();
  #pragma omp parallel
  {
    #pragma omp single
    {
      matmul_depend(N, bs, a, b, c);
    }
  }
  end = omp_get_wtime();
  printf("Work took %f seconds\n", end - start);
}
