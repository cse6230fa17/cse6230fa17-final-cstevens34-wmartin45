#if !defined(COMMON_H)
#define      COMMON_H

static inline size_t sz_min(size_t x, size_t y) {
  return (x < y) ? x : y;
}

#endif
