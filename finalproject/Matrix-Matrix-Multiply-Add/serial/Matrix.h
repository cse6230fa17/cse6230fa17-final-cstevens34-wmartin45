#if !defined(MATRIX_H)
#define      MATRIX_H

#include <stddef.h>
#include <memory>
#include <cstring>

template <typename T> class Matrix 
{
public:
    Matrix(int rows, int cols) : _rows(rows), _cols(cols), _data(new T[rows * cols]) 
    {
        std::memset(_data.get(), 0, rows*cols * sizeof(T));
    }

    Matrix(int rows, int cols, const T* data) : _rows(rows), _cols(cols), _data(new T[rows * cols]) 
    {
        std::memcpy(_data.get(), data, rows*cols*sizeof(T));
    }

    Matrix(int rows, int cols, T* data) : _rows(rows), _cols(cols), _data(new T[rows * cols]) 
    {
        std::memcpy(_data.get(), data, rows*cols*sizeof(T));
    }

    Matrix(const Matrix& m) : _rows(m._rows), _cols(m._cols), _data(m._data) {}

    T* operator[](unsigned int row) const { return _data.get() + row * _cols; }

    inline Matrix& operator += (const Matrix& m)
    {
        for (int r = 0; r < rows(); r++)
        {
            for (int c = 0; c < cols(); c++)
            {
                (*this)[r][c] += m[r][c];
            }
        }

        return *this;
    }

    inline Matrix& operator *= (T scalar)
    {
        for (int r = 0; r < rows(); r++)
        {
            for (int c = 0; c < cols(); c++)
            {
                (*this)[r][c] *= scalar;
            }
        }

        return *this;
    }

    unsigned int rows() const { return _rows; }
    unsigned int cols() const { return _cols; }

private:
    unsigned int _rows;
    unsigned int _cols;
    std::shared_ptr<T> _data;
};


template <typename T> Matrix<T> operator +(const Matrix<T>& a, const Matrix<T>& b)
{
    Matrix<T> result(a.rows(), b.cols());
    for (int r = 0; r < result.rows(); r++)
    {
        for (int c = 0; c < result.cols(); c++)
        {
            result[r][c] = a[r][c] + b[r][c];
        }
    }
    return result;
}

template <typename T> Matrix<T> operator *(const Matrix<T>& a, const Matrix<T>& b)
{
    Matrix<T> result(a.rows(), b.cols());
    for (int r = 0; r < result.rows(); r++)
    {
        for (int c = 0; c < result.cols(); c++)
        {
            // if cc > r cc -= r;
            for (int col = 0; col < b.rows(); col++)
            {
                result[r][c] += a[r][col] * b[col][c];
            }
        }
    }

    return result;
}

template <typename T> Matrix<T> operator *(const Matrix<T>& a, T scalar)
{
    Matrix<T> result(a.rows(), a.cols(), &a[0][0]);

    for (int r = 0; r < result.rows(); r++)
    {
        for (int c = 0; c < result.cols(); c++)
        {
            result[r][c] *= scalar;
        }
    }

    return result;
}


#endif
