\documentclass[12pt]{article}
\usepackage{amsmath,amssymb}
\usepackage[margin=1in]{geometry}
\usepackage[ruled,lined,linesnumbered,noend]{algorithm2e}
\title{Bitonic Sort: Parallel Analysis}
\author{Toby Isaac}
\date{CSE 6230, Fall 2017}

\newcommand{\lgtwo}{\log_2}
\newcommand{\half}{{\textstyle \frac{1}{2}}}

\begin{document}
\maketitle

\section{Pseudocode}

\begin{algorithm}
  \caption{%
    \texttt{sortBitonic}($r$, $P$, $n$, $A$, $d$, $b$)%
  }%
  \KwData{%
    $r$ is the process's rank; \\
    $P$ is the number of processes (power of 2); \\
    $n$ is the number of local keys (same on all processes); \\
    $A$ is the array of keys; \\
    $d$ is {\tt True} if sorting forward, {\tt False} if sorting backward; \\
    $b$ is {\tt True} if $A$ is bitonic on input.%
  }%

  \eIf{$P = 1$}{%
    \eIf{$b$}{%
      \tcc{local sort when input is bitonic, $O(n \lgtwo n)$}
      \texttt{sortLocalBitonic}($n$, $A$, $d$)
    }{%
      \tcc{local sort with no special condition, $O(n \lgtwo n)$}
      \tcc{(but we expect slower than sortLocalBitonic())}
      \texttt{sortLocal}($n$, $A$, $d$)
    }%
  }{%
    \If{not $b$}{%
      \tcc{Make $A$ bitonic, sort half forward, half backward}
      \tcc{$\otimes$ is exclusive or}
      \texttt{sortBitonic}($r \mod P / 2$, $P/2$, $n$, $A$, $d \otimes (r \geq P
      / 2)$, \texttt{False})
    }%
    \tcc{Swap splits keys into lesser and greater halves}
    \tcc{(or greater and lesser if backward direction)}
    \texttt{swapBitonic}($r$, $P$, $n$, $A$, $d$)

    \tcc{Recursive sort of distributed bitonic sequence}
    \texttt{sortBitonic}($r \mod P / 2$, $P / 2$, $n$, $A$, $d$, \texttt{True})
  }%
\end{algorithm}

\begin{algorithm}
  \caption{%
    \texttt{swapBitonic}($r$, $P$, $n$, $A$, $d$)%
  }%
  \KwData{%
    $r$ is the process's rank; \\
    $P$ is the number of processes (power of 2); \\
    $n$ is the number of local keys (same on all processes); \\
    $A$ is the array of keys; \\
    $d$ is {\tt True} if sorting forward, {\tt False} if sorting backward.
  }%
  \tcc{Communicating partner in the other half}
  $q \leftarrow r + P/2 \mod P$

  $A_{\text{recv}} \leftarrow$ \texttt{Sendrecv}($A$, $n$, $q$)

  \tcc{$\otimes$ is exclusive or}
  \eIf{$(r < q) \otimes d$}{%
    \tcc{Swap to get lesser value}
    \For{$0 \leq i < n$}{%
      $A[i] \leftarrow \min\{A[i],A_{\text{recv}}[i]\}$
    }%
  }{%
    \tcc{Swap to get greater value}
    \For{$0 \leq i < n$}{%
      $A[i] \leftarrow \max\{A[i],A_{\text{recv}}[i]\}$
    }%
  }%
\end{algorithm}

\section{Runtime analysis}

We want an expression for $T_b(n,p)$, the runtime of parallel bitonic sort in
terms of $n$, the number of local keys, and $P$, the number of processes.

We will first get $\tilde{T}_b(n,p)$, the runtime of parallel sort when the
input is bitonic ($b$ is \texttt{True}).
In this case
%
\[
  \tilde{T}_b(n,1) = T_{lb}(n) := T_{\texttt{sortLocalBitonic}}(n) \in O(n
  \lgtwo n).
\]
%
Then, the recursive case:
%
\[
  \tilde{T}_b(n,P) = (T_{sb}(n,P) := T_{\texttt{swapBitonic}}(n,P)) +
  \tilde{T}_b(n,P/2).
\]
%
We will assume that network congestion is not a problem so that $T_{sb}(n,P)$
is independent of $P$, $T_{sb}(n)$.  Ignoring for a moment the cost of local
operations in \texttt{swapBitonic} (the loops at lines 4 and 7), the
cost is at least the cost of a \texttt{Sendrecv} of $n$ keys.  In the
parameters of the \texttt{LogGP} model, the cost of a \texttt{Sendrecv} should
be
%
\[
  T_{sb}(n) \geq L + 2o + G(kn - 1),
\]
%
where $L$ is the network latency, $o$ is the overhead of starting or finishing
a communication, $G$ is the inverse of the network bandwidth (i.e., $G$ has
units of s/B), and $k$ is the number of bytes per key (8, in our case).

Expanding the recursion, we get
%
\[
  \begin{aligned}
    \tilde{T}_b(n,P) &= \underbrace{T_{sb}(n) + \dots + T_{sb}(n)}_{\times \lgtwo
    P} + \tilde{T}_b(n,1) \\
    &= T_{sb}(n)\lgtwo P + T_{lb}(n).
  \end{aligned}
\]

Now we solve for the full runtime $T_b(n,P)$ when the inputs are not bitonic
($b$ is \texttt{False}), the default case.

The base case:
%
\[
  T_b(n,1) = T_{l}(n) := T_{\texttt{sortLocal}}(n) \in O(n
  \lgtwo n).
\]
%
The recursive case:
%
\[
  \begin{aligned}
    T_b(n,P) &= T_b(n,P/2) + \tilde{T}_b(n,P) \\
    &=
    \underbrace{\tilde{T}_b(n,P) + \tilde{T}_b(n,P/2) + \dots + 
    \tilde{T}_b(n,2)}_{\times \lgtwo P} + T_b(n,1).
  \end{aligned}
\]
%
We insert our expression for $\tilde{T}_b(n,P)$, group like terms, and 
use the property of logarithms to turn $\lgtwo P / K$ into $\lgtwo P - \lgtwo K$:
%
\[
  \begin{aligned}
    T_b(n,P)
    =
    \ &T_{sb}(n) (
    \lgtwo P + \lgtwo P/2 + \dots + \lgtwo 2
    )\ +
    \\
    &T_{lb}(n) (
      \underbrace{%
        1 + 1 + \dots + 1
      }_{\times \lgtwo P}%
    ) + T_b(n,1)
    \\
    =
    \ &T_{sb}(n) (
      \underbrace{%
        \lgtwo P + \lgtwo P + \dots + \lgtwo P
      }_{\times \lgtwo P}%
    )\ -
    \\
    &T_{sb}(n) (
        0 + 1 + \dots + (\lgtwo P - 1)
    )\ +
    \\
    &T_{lb}(n) (
      \underbrace{%
        1 + 1 + \dots + 1
      }_{\times \lgtwo P}%
    ) + T_b(n,1)
    \\
    =
    \ &T_{sb}(n)\left(\lgtwo^2 P - \sum_{i=1}^{\lgtwo P - 1} i\right)\  +
    T_{lb}(n) \lgtwo P + T_l(n) \\
    =
    \ &T_{sb}(n)(\lgtwo^2 P - \half(\lgtwo P - 1)\lgtwo P)
    + T_{lb}(n) \lgtwo P + T_l(n) \\
    =
    \ &\half T_{sb}(n)(\lgtwo^2 P + \lgtwo P)
    + T_{lb}(n)\lgtwo P + T_l(n).
  \end{aligned}
\]

We now have a runtime expression for the full \texttt{sortBitonic}() function
in terms of its components that are simpler to analyze/measure on their own:
\texttt{sortLocal}(), \texttt{sortLocalBitonic}(), and \texttt{swapBitonic}(),
which we take to be $O(n \log n)$, $O(n \log n)$ and $O(n)$, respectively.  If
we trust our models of the runtimes of those components, then our model lets
us:
%
\begin{itemize}
  \item
    Predict the performance of \texttt{sortBitonic}().
  \item
    Verify and validate the performance of \texttt{sortBitonic}().  If our
    measured times do not match the model, the implementation is wrong or the
    model is wrong (e.g., our pseudocode omits something critical).
  \item
    Speculatively compare the performance of other sort implementations to
    \texttt{sortBitonic}().
\end{itemize}
%

If we \emph{don't} already have good models of the component algorithms, we
can use the functional form of $T_b(n,P)$ to define an empirical model.

\section{Empirical Model}

First, let us approximate the components with some as yet undetermined
coefficients:
%
\[
  \begin{aligned}
    T_{sb}(n) &\approx C^0_{sb} + C^1_{sb}\ n,\\
    T_{lb}(n) &\approx C_{lb}\ n \lgtwo n,\\
    T_{l}(n) &\approx C_{l}\ n \lgtwo n.
  \end{aligned}
\]

[We include the constant term $C^0_{sb}$ because we think network latency may be
significant in the case of small $n$.]

Expanding $T_b(n,P)$:
%
\[
  T_b(n,P) \approx \half (C^0_{sb} + C^1_{sb}n) (\lgtwo^2 P + \lgtwo P)
  + C_{lb}\ n\lgtwo n \lgtwo P
  + C_{l}\ n \lgtwo n.
\]
%

Now we could measure the runtime of $\texttt{sortBitonic}(n,P)$ for a large
number of $(n,P)$ pairs, and do least-squares fit for the coefficients, and
use a statistical measure of goodness of fit to decide if our four-coefficient
model is useful.

We can also isolate some of the coefficient with subsets of the parameter
space.  If $P=1$, then $C_l$ (the coefficient for the local sort) is the only
one that has an effect, so we can estimate $C_l$ in isolation.  If $N=1$, then
we can estimate $(C_{sb}^0 + C_{sb}^1)$ (the minimum cost of performing
\texttt{swapBitonic}()).





\end{document}
