# Notes on Pittsburgh Supercomputing Center's Bridges

- [User Guide](https://www.psc.edu/bridges/user-guide/)
    - You can log in through gsissh via xsede, but a username/password for
      connecting directly was generated for you.  I don't know what your
      username is, but if you connect from portal you can see it.
    - To change the password that was automatically generated for you,
      [go here](https://www.psc.edu/bridges/user-guide/connecting-to-bridges#password).  Now you can `ssh username@bridges.psc.edu`.
    - ssh keys can be added, but not just directly into `~/.ssh`, you have to
      use [their web interface](https://grants.psc.edu/cgi-bin/ssh/listKeys.pl).
    - `module load cuda` to make `nvcc` available: `nvcc` syntax is like the compilers we've seen: `nvcc -O -o prog prog.cu`


